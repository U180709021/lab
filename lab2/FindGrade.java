public class FindGrade {
    public static void main(String[] args){
        int value1=Integer.parseInt(args[0]);
    
    if (value1<60 && value1>=0)
        System.out.println("Your Grade is F");
    else if (value1<70 && value1>=60)
        System.out.println("Your Grade is D");
     else if (value1<80 && value1>=70)
        System.out.println("Your Grade is C");
     else if (value1<90 && value1>=80)
        System.out.println("Your Grade is B");
     else if (value1<=100 && value1>=90)
        System.out.println("Your Grade is A");
    else if (value1>100 || value1<0)
        System.out.println("It's not a valid score!");
}
}

