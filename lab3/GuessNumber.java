import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");

//		System.out.println(number);

//		int guess = reader.nextInt(); //Read the user input
		int attempt = 1;

		while(true) {
			int guess = reader.nextInt();
//			reader.nextLine();
			if (number == guess) {
				System.out.println("Congratulations! You won after " + attempt + (attempt==1 ? " attempt!" : " attempts!"));
				break;
			} else if (guess == -1){
				System.out.println("The number was " + number);
				System.out.println("Goodbye");
				break;
			}

			attempt++;

			if(number > guess) {
				System.out.println("Mine is greater than your guess");
			} else {
				System.out.println("Mine is less than your guess");
			}
			System.out.println("Sorry!\nType -1 to quit or guess another");
		}
		
		reader.close(); //Close the resource before exiting
	}
	
	
}
